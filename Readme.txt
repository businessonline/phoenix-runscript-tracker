runscript_tracker.py requires the GSpread-0d215f2b857b.json authentication file

call the script using 'python runscript_tracker.py --client-name xyz --status success (or failed, or any string)

The script will populate the 'last run status' column of the client row of the 'RunScript Tracker' workbook 'Run Script Tracker' worksheet with a string "the entered status - the date"

