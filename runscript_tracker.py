import os.path
import re
import platform
import argparse

### NOTE: You must share the G Sheet with gspread@gspread-1275.iam.gserviceaccount.com to allow the API to access the sheet.
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from datetime import datetime

class Tracker(object):


	def __init__(self, client=None, status = None):
		"""
		This function initializes the variables.
		"""

		# Date
		self.date = datetime.utcnow().strftime('%Y-%m-%d')
		# Set variables
		self.status = status
		self.clientName = client


	def execute(self):	
		"""
	    https://docs.google.com/a/businessol.com/spreadsheets/d/1xUbSIGgU1VAeGZPEoL9Ymfc0LWgHbWvvaTjhT8GSh20/edit?usp=sharing
		"""

		scope = ['https://spreadsheets.google.com/feeds']

		credentials = ServiceAccountCredentials.from_json_keyfile_name('GSpread-0d215f2b857b.json', scope)

		gc = gspread.authorize(credentials)

		#my_workbook = gc.open("Search Query Parameter Lookup") ### title of the G. Spreadsheet
		tracker_workbook = gc.open("RunScript Tracker") ### title of the G. Spreadsheet

		tracker_worksheet = tracker_workbook.worksheet("Run Script Tracker") ### name of worksheet to pull data from
		status_column_index = 0
		client_column_index = 0
		client_row_number = 0
		status_value = self.status + " - " + self.date
		# Client Column name in spreadsheet is 'client'


		g_sheet_data = tracker_worksheet.get_all_values()

		# Set array of g sheet
		column_arr = []
		column_arr = g_sheet_data[0]

		# Set column indexs for 'last run status' column and 'client' column
		status_column_index = column_arr.index("last run status") + 1
		client_column_index = column_arr.index("client") + 1
		 
		# Get 'client' column as a list
		clients_list = tracker_worksheet.col_values(client_column_index)
		# Get row from number from list
		client_row_num = clients_list.index(self.clientName) + 1

		### sheet.update_cell(row, column, value str)
		tracker_worksheet.update_cell(client_row_num,status_column_index,status_value)

	def run(self):
		trackerObj.execute()
	

if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Write to runscript tracker the pipeline success or failure')
	parser.add_argument('--client-name', required=True, help='The client short-name (ex. whitecap)')
	parser.add_argument('--status',  help='The pipeline status...failed or success')
	
	args = parser.parse_args()
	trackerObj = Tracker(client=args.client_name,status=args.status)
	trackerObj.execute()

	#trackerObj = Tracker(client='cap',status='success')
	#trackerObj.execute()
